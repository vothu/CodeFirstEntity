﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeFirstEntity.Entity;

namespace CodeFirstEntity.Reponsitory
{
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        T GetById(object id);
        void Create(T obj);
        void Update(T obj);
        void Delete(object id);
        
    }
}