﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CodeFirstEntity.Entity;

namespace CodeFirstEntity.Reponsitory
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly string table =  nameof(T);
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;

        public Repository(DbContext context) 
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
        
            var query = @"select * from @p0";
            return _dbSet.SqlQuery(query, table);

        }

        public T GetById(object id)
        {
            var query = @"select * from @p0 where Id = @p1";
            return _dbSet.SqlQuery(query, table,id).FirstOrDefault();
        }

        public void Create(T obj)
        {
            _dbSet.Add(obj);
            _context.SaveChangesAsync();
        }

        public void Update(T obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChangesAsync();
        }

        public void Delete(object id)
        {
            var query = @"delete @p0 where Id = @p1";
            _context.Database.ExecuteSqlCommandAsync(query, table, id);
        }

        
    }
}