﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CodeFirstEntity.Entity;
using CodeFirstEntity.IService;
using CodeFirstEntity.Reponsitory;

namespace CodeFirstEntity.Controllers
{
    [RoutePrefix(("api/v1/Customer"))]
    public class CustomerController : ApiController
    {
        private readonly ICustomerService _customerService;
        private readonly IRepository<Customer> _repository;
        public CustomerController(ICustomerService customerService, IRepository<Customer> repository)
        {
            _customerService = customerService;
            _repository = repository;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCustommer([FromUri] int id)
        {
            var customer =   _repository.GetById(id);
            if (customer == null) return NotFound();
            return Ok(customer);


        }
    }
}
