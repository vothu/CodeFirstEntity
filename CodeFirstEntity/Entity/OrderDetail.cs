﻿namespace CodeFirstEntity.Entity
{
    public class OrderDetail:BaseEntity
    {
       
        public int Total { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }

    }
}