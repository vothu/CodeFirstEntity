﻿using System.Collections.Generic;

namespace CodeFirstEntity.Entity
{
    public class Order:BaseEntity
    {

        public ICollection<OrderDetail> OrderDetails { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public Staff Staff { get; set; }
        public int StaffId { get; set; }
    }
}