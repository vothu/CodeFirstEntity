﻿using System;
using System.Collections.Generic;

namespace CodeFirstEntity.Entity
{
    public class Customer : BaseEntity
    {
        public int Name { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public DateTime BirthDay { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}