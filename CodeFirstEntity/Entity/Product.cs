﻿using System.Collections.Generic;

namespace CodeFirstEntity.Entity
{
    public class Product:BaseEntity
    {
       
        public string Name { get; set; }
        public int Price { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}