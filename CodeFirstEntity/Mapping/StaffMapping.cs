﻿using System.Data.Entity.ModelConfiguration;
using CodeFirstEntity.Entity;

namespace CodeFirstEntity.Mapping
{
    public class StaffMapping : EntityTypeConfiguration<Staff>
    {
        public StaffMapping()
        {
            HasKey(t => t.Id);
            ToTable("Staff");
        }
    }
}