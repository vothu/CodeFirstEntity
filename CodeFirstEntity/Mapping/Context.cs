﻿using System.Data.Entity;
using CodeFirstEntity.Entity;

namespace CodeFirstEntity.Mapping
{
    public class Context :DbContext
    {
        public Context(): base("CodeFirstEntity")
        {
            
        }

        public Context(string connectionStrings) : base(connectionStrings)
        {
            
        }
        public DbSet<Staff> Staff { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Customer> Customer { get; set; }
    }
}