﻿using System.Data.Entity.ModelConfiguration;
using CodeFirstEntity.Entity;

namespace CodeFirstEntity.Mapping
{
    public class ProductMapping : EntityTypeConfiguration<Product>
    {
        public ProductMapping()
        {
            HasKey(t => t.Id);
            ToTable("Product");
        }
    }
}