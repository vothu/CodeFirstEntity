﻿using System.Data.Entity.ModelConfiguration;
using CodeFirstEntity.Entity;

namespace CodeFirstEntity.Mapping
{
    public class OrderDetailMapping : EntityTypeConfiguration<OrderDetail>
    {
        public OrderDetailMapping()
        {
            HasKey(t => t.Id);
            ToTable("OrderDetail");
            HasRequired(m => m.Product).WithMany(m => m.OrderDetails).HasForeignKey(m => m.ProductId);
            HasRequired(m => m.Order).WithMany(m => m.OrderDetails).HasForeignKey(m => m.OrderId);
        }
    }
}