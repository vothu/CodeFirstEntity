﻿using System.Data.Entity.ModelConfiguration;
using CodeFirstEntity.Entity;

namespace CodeFirstEntity.Mapping
{
    public class OrderMapping : EntityTypeConfiguration<Order>
    {
        public OrderMapping()
        {
            HasKey(t => t.Id);
            ToTable("Order");
            HasRequired(m => m.Customer).WithMany(m => m.Orders).HasForeignKey(m => m.CustomerId);
            HasRequired(m => m.Staff).WithMany(m => m.Orders).HasForeignKey(m => m.StaffId);
        }
    }
}