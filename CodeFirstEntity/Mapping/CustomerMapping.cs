﻿using System.Data.Entity.ModelConfiguration;
using CodeFirstEntity.Entity;

namespace CodeFirstEntity.Mapping
{
    public class CustomerMapping : EntityTypeConfiguration<Customer>
    {
        public CustomerMapping()
        {
            HasKey(t => t.Id);
            ToTable("Customer");
        }
    }
}