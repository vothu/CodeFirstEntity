﻿using System.Reflection;
using System.Web.Http.SelfHost;
using Autofac;
using Autofac.Integration.WebApi;
using CodeFirstEntity.Controllers;
using CodeFirstEntity.IService;
using CodeFirstEntity.Service;
using Owin;

namespace CodeFirstEntity
{
    /// <summary>
    /// AutofacConfig
    /// </summary>
    public class AutofacConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static ContainerBuilder Configuration(IAppBuilder app)
        {
            //var builder = new ContainerBuilder();
            //builder.RegisterControllers(typeof(WebApiApplication).Assembly);
            //builder.RegisterControllers(typeof(ApiController).Assembly);
            //builder.RegisterApiControllers(typeof(WebApiApplication).Assembly);
            //builder.RegisterApiControllers(typeof(ApiController).Assembly);

            ////builder.RegisterType<CustomerService>().As<ICustomerService>().InstancePerLifetimeScope();
            //builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).AsImplementedInterfaces().InstancePerRequest();
            //return builder;


            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            //var config = GlobalConfiguration.Configuration;

            var config = new HttpSelfHostConfiguration("http://localhost:56275");
            //builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            // OPTIONAL: Register the Autofac model binder provider.
            builder.RegisterWebApiModelBinderProvider();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // You can register controllers all at once using assembly scanning...
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // ...or you can register individual controlllers manually.
            builder.RegisterType<CustomerController>().InstancePerRequest();

            builder.RegisterType<CustomerService>().As<ICustomerService>().InstancePerDependency();
            return builder;


            

        }



    }
}